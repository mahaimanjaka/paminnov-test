
import React,{useState} from 'react';
import Axios from 'axios';
import {Link, useHistory} from 'react-router-dom';

function RegistreUser() {


    const [nom, setNom] = useState('');
    const [prenom, setPrenom] = useState('');
    const [courriel, setCourriel] = useState('');
    const [motPasse, setMotPasse] = useState('');
    const [motPasseConfir, setMotPasseConfir] = useState('');

    const history = useHistory();

    const registreUser =(event) => {
        event.preventDefault();
        if(motPasse == motPasseConfir){
            Axios.post("http://localhost:8080/api/users/registre", 
                {
                    nom: nom,
                    prenom: prenom,
                    courriel: courriel, 
                    motPasse: motPasse
                }).then((response) => {
                    alert("Enregistrement terminé!");
                    console.log(response.data);
                    history.push('/login');
            }).catch(function (error) {
                if (error.response) {
                  alert(error.response.data.erreur)
                  console.log(error.response.data);
                } else if (error.request) {
                    alert(error.request)
                  console.log(error.request);
                } else {
                  alert(error.message);
                  console.log('Error', error.message);
                }
            });
        }else{
            alert("Les nouveaux mots de passes ne sont pas identique");
        }
        
    };


  return (
    <div className="App">
        <div className= "row">
            <div className="col-md-6" style={{marginLeft:'380px',marginTop:'220px'}}>
                <div className="card">
                    <div className="card-header card-header-primary">
                        <h3 className="card-title">Creation de compte</h3>
                        
                    </div>
                    <div className="card-body">
                        <form>
                            <div className="row">
                                <div className="col-md-10">
                                    <div className="form-group bmd-form-group">
                                        <label className="bmd-label-floating">Nom:</label>
                                        <input type="text" className="form-control" onChange= {(e) => {setNom(e.target.value);}} name="nom" style={{paddingLeft:'120px'}}/>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-10">
                                    <div className="form-group bmd-form-group">
                                        <label className="bmd-label-floating">Prenom:</label>
                                        <input type="text" className="form-control" onChange= {(e) => {setPrenom(e.target.value);}} name="prenom" style={{paddingLeft:'120px'}}/>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-10">
                                    <div className="form-group bmd-form-group">
                                        <label className="bmd-label-floating">Email:</label>
                                        <input type="text" className="form-control" onChange= {(e) => {setCourriel(e.target.value);}} name="courriel" style={{paddingLeft:'120px'}}/>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-10">
                                    <div className="form-group bmd-form-group">
                                        <label className="bmd-label-floating">Mots de pass:</label>
                                        <input type="password" className="form-control" onChange= {(e) => {setMotPasse(e.target.value);}} name="motPasse" style={{paddingLeft:'120px'}}/>
                                    </div>
                                </div>
                            </div>
                            
                            <div className="row">
                                <div className="col-md-10">
                                    <div className="form-group bmd-form-group">
                                        <label className="bmd-label-floating">Confirmation: </label>
                                        <input type="password" className="form-control" onChange= {(e) => {setMotPasseConfir(e.target.value);}} name="motPasseConfir" style={{paddingLeft:'120px'}} />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-3">
                                    <button type="submit" onClick={registreUser} className="btn btn-primary pull-right">Enregistrer</button>
                                </div>
                                <div className="col-md-4">
                                </div>
                                <div className="col-md-4">
                                   <Link to= '/login'>
                                        <button type="button" className="btn btn-secondary pull-right">Annuler</button>
                                   </Link>
                                    
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
         
    </div>
  );
}

export default RegistreUser;
