import React,{useEffect,useState} from 'react';
import {Link} from 'react-router-dom';
import Axios from 'axios';
import '../App.css';
//import TemplateModal from './UpdateLivre';



function Livre() {

  const [livreList, setLivreList] = useState([]);
  
//---------- Get liste Livre --------------------
  useEffect(() => {
      Axios.get("http://localhost:8080/api/livres/listLivre").then((response) => {
        setLivreList(response.data);
        //console.log(response);
      })
  },[]);

  return (
    <div className="content">
      <div className="container-fluid">
        <div className="card card-plain" style={{marginTop:'-70px'}}>
            <div className="card-header card-header-primary">
              <h4 className="card-title mt-0"> List des livres</h4>
              <p className="card-category"> public et privé</p>
            </div>
            <div className="card-body">
              <div className="divTable">
                <table className="table table-hover">
                  <thead >
                    <tr>
                      <th>Titre </th>
                      <th>Description </th>
                      <th>Auteur</th>
                      <th>Type </th>
                      <th>Date de publication </th>
                      <th>Couverture </th>
                      <th> </th>
                    </tr>
                  </thead>
                  <tbody>
                  {livreList.map(function (val) {
                      return (<tr  key={val.id}><td>{val.titre}</td>
                        <td>{val.description}</td>
                        <td> {val.User.nom} {val.User.prenom}</td>
                        <td>{val.type}</td>
                        <td>{val.datePub}</td>
                        <td>
                            <img style={{width: 70, height: 110, borderWidth: 1, borderColor: 'red'}} src={val.couverture}/>
                        </td>
                        <td>
                          <Link to={`/editLivre/${val.id}`}><span class="input-group-btn" style={{cursor: 'pointer'}}><i class="material-icons">save</i></span></Link>
                          <Link><span class="input-group-btn" style={{cursor: 'pointer'}}><i class="material-icons">delete</i></span>
                          </Link>
                        </td>
                      </tr> )
                      })
                    }
          
                    </tbody>
                </table>
              </div>
              <div className="card-footer">
                <Link to ="/newLivre" >
                  <button to ="" type="button" className="btn btn-primary pull-right" >Publier nouvel livre</button>
                </Link>
               
              </div>


            </div>
          </div>
      </div>
    </div>
  );
}

export default Livre;
