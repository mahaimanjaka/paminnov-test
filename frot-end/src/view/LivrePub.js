import React,{useEffect,useState} from 'react';
import {Link} from 'react-router-dom';
import Axios from 'axios';

function LivrePub() {


  const [livreList, setLivreList] = useState([]);
  useEffect(() => {
      Axios.get("http://localhost:8080/api/livres/listLivrePub").then((response) => {
        setLivreList(response.data);
        console.log(response);
      })
  },[]);



  return (
    <div className="container-fluid">
      <div className="card card-plain">
          <div className="card-header card-header-primary">
            <h4 className="card-title mt-0"> List des livres</h4>
            <p className="card-category"> public et privé</p>
          </div>
          <div className="card-body">
            <div className="table-responsive">
              <table className="table table-hover">
                <thead className="">
                  <tr>
                    <th>Titre </th>
                    <th>Description </th>
                    <th>Auteur  </th>
                    <th>Type  </th>
                    <th>Date de publication </th>
                    <th>Couverture </th>
                  </tr>
                </thead>
                <tbody>
                {livreList.map(function (val) {
                     return (<tr key={val.id}><
                      td>{val.titre}</td>
                      <td>{val.description}</td>
                      <td> {val.User.nom} {val.User.prenom}</td>
                      <td>{val.type}</td>
                      <td>{val.datePub}</td>
                      <td>
                           <img style={{width: 70, height: 110, borderWidth: 1, borderColor: 'red'}} src={val.couverture}/>
                      </td>
                    </tr> )
                    })
                  }
                  </tbody>
              </table>
            </div>
            <div className="card-footer">
              <Link to ="/registre" >
                <button type="submit" className="btn btn-primary pull-right" >S'inscrire pour voir tous les livres</button>
              </Link>
            </div>

          </div>
        </div>
    </div>
  );
}

export default LivrePub;
