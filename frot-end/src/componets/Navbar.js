import React from 'react';
import { useHistory } from 'react-router-dom';
import Cookies from 'js-cookie';

function Navbar() {

  const history = useHistory();

  const logOut = ()=>{
    history.push('/login');
    Cookies.remove('courriel',{ path: '/' });
    Cookies.remove('userId',{ path: '/' });
    Cookies.remove('nom',{ path: '/' });
    Cookies.remove('prenom',{ path: '/' });
  }
  
  var nom = Cookies.get('nom');
  var prenom = Cookies.get('prenom');
  var courriel = Cookies.get('courriel');

  return (
    <nav className="navbar navbar-expand navbar-dark bg-primary flex-column flex-md-row bd-navbar ">
        <div className="container-fluid">
          <div className="navbar-wrapper">
            <h4 className="navbar-brand" >{nom} {prenom}  </h4>
          </div>
          <button className="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span className="sr-only">Toggle navigation</span>
            <span className="navbar-toggler-icon icon-bar"></span>
            <span className="navbar-toggler-icon icon-bar"></span>
            <span className="navbar-toggler-icon icon-bar"></span>
          </button>
          <div className="collapse navbar-collapse justify-content-end">
            
            <ul className="navbar-nav">
               <h5>{courriel}</h5>
                <i className="material-icons" onClick={logOut}>login</i>
              
            </ul>
          </div>
        </div>
      </nav>
  );
}

export default Navbar;
