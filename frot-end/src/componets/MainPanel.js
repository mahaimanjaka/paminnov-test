import React from 'react';
import NavBar from './Navbar';
import Content from './Content';
import Footer from './Footer';
import {BrowserRouter} from 'react-router-dom';
import Menu from './Menu';

function App() {
  return (
    <div className="main-panel">
      <NavBar/>
      <BrowserRouter>
        <Menu/>
        <Content/>
        <Footer/>
      </BrowserRouter>
    </div>
  );
}

export default App;
