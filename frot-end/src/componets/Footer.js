import React from 'react';

function Footer() {
  return (
    <footer className="footer" style={{marginTop:'-120px'}}>
        <div className="container-fluid">
          <nav className="float-left">
            
          </nav>
          <div className="copyright float-right">
            ©
            <script>
              document.write(new Date().getFullYear())
            </script>2020, test <i className="material-icons">favorite</i> by Njaka
          </div>
        </div>
      </footer>
  );
}

export default Footer;
