import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Login from './view/Login';
import LivrePub from './view/LivrePub';
import Registre from './view/RegistreUser';
import InitialPass from './view/InitialPass';
import MainPanel from './componets/MainPanel';
import ConfirmedPass from './view/ConfirmedPass'

function App() {
  return (
    <BrowserRouter>
      <Switch>
          <Route exact  path="/" component={Login}/>
          <Route  path="/acceuil" component={MainPanel}/>
          <Route  path="/login" component={Login}/>
          <Route  path="/livrePub" component={LivrePub}/>
          <Route  path="/registre" component={Registre}/>
          <Route  path="/initialPass" component={InitialPass}/>
          <Route  path="/confirmedPass" component={ConfirmedPass}/>
      </Switch>
    </BrowserRouter>
  );
}

export default App;

 