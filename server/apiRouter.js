// importe
const express = require('express');
const userCtrl = require('./controllers/userCtrl');
const livreCtrl = require('./controllers/livreCtrl')

//Routeur
exports.router = (function() {
    var apiRouter = express.Router();
    
    // User route
    apiRouter.route('/users/registre/').post(userCtrl.registre);
    apiRouter.route('/users/login/').post(userCtrl.login);
    apiRouter.route('/users/me/').get(userCtrl.getUserActif);
    apiRouter.route('/users/me/').put(userCtrl.updateUser);
    apiRouter.route('/users/listUser/').get(userCtrl.getListUser);
    apiRouter.route('/livres/enregistrer/').post(livreCtrl.enregistrerLivre);
    apiRouter.route('/livres/listLivre/').get(livreCtrl.getListLivre);
    apiRouter.route('/livres/listLivrePub/').get(livreCtrl.getListLivrePub);
    apiRouter.route('/livres/listOneLivre/:id').get(livreCtrl.getOneLivre);
    apiRouter.route('/livres/updateLivre/:id').put(livreCtrl.updateLivre);
    return apiRouter;
})();