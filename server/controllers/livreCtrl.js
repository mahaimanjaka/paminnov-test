
var models = require('../models');
var asyncLib = require('async');
var Sequelize = require('sequelize');

//Routes
module.exports = {
    //Enregistrer livre
    enregistrerLivre: function (req,res){
        // param
        var titre = req.body.titre;
        var description = req.body.description;
        var userId = req.body.id_user;
        var datePublication = req.body.datePub;
        var type = req.body.type;
        var couverture = req.body.couverture;
        var obj ={titre, description, userId, datePublication, type, couverture};
        console.log(obj);
        //verifier le parametre null
        if(titre == "" || description == ""|| datePublication == ""|| type == ""|| couverture == ""){
            return res.status(400).json({'erreur':' parammetre vide'});
        } else{
            asyncLib.waterfall([
                //founction creer nouveau livre
                function(donne){
                    var newLivre = models.Livres.create({
                        titre: titre,
                        description:description,
                        id_user:userId,
                        datePub: datePublication,
                        type:type,
                        couverture: couverture
                    })
                    .then(function(newLivre){
                        
                        donne(newLivre);
                    })
                    .catch(function(err){
                        return res.status(500).json({'erreur':'Ajout du livre imposible'});
                    });
                }
            ],function(newLivre){
                    if(newLivre){
                        return res.status(201).json({ 'livreId': newLivre.id });
                    }else{
                        return res.status(500).json({'erreur':'Ajout du livre imposible'});
                    }
                }
            );

        }

        
    },
        //fonction lister tout le livre
    
        getListLivre: function(req,res){
          models.Users.hasMany(models.Livres, {foreignKey: 'id_user'});
          models.Livres.belongsTo(models.Users, {foreignKey: 'id_user'});
          models.Livres.findAll({
              attributes: { exclude: ['UserId'] },
             include: [{
                  model: models.Users,
                  required: true
                }
              ]
      })
          .then(function(listLivre) {
              if (listLivre) {
                  res.status(201).json(listLivre);
              } else {
                  console.log(listLivre)
                  res.status(404).json({ 'erreur': 'list livre non touver' });
              }
              })
          .catch(function(err) {
              res.status(500).json({ 'erreur': 'Impossible de récupérer list' });
          });
      },

      getOneLivre: function(req,res){
        models.Livres.findOne({
            attributes: ['titre','description',[Sequelize.fn('date_format', Sequelize.col('datePub'), '%Y-%m-%d'), 'datePub'],'type'],
            where:{'id': req.params.id}
    })
        .then(function(listLivre) {
            if (listLivre) {
                res.status(201).json(listLivre);
            } else {
                console.log(listLivre)
                res.status(404).json({ 'erreur': 'list livre non touver' });
            }
            })
        .catch(function(err) {
            res.status(500).json({ 'erreur': 'Impossible de récupérer list' });
        });
    },

    //fonction lister tout le livre public
    getListLivrePub: function(req,res){
        models.Users.hasMany(models.Livres, {foreignKey: 'id_user'});
        models.Livres.belongsTo(models.Users, {foreignKey: 'id_user'});
        models.Livres.findAll({
          where:{type:'Public'},
          attributes: { exclude: ['UserId'] },
           include: [{
                model: models.Users,
                required: true
              }
            ]
           
    })
        .then(function(listLivre) {
            if (listLivre) {
                res.status(201).json(listLivre);
            } else {
                console.log(listLivre)
                res.status(404).json({ 'erreur': 'list livre non touver' });
            }
            })
        .catch(function(err) {
            res.status(500).json({ 'erreur': 'Impossible de récupérer list' });
        });
    },

    updateLivre: function(req, res) {
        // Get auth header
        var headerAuth  = req.headers['authorization'];
        //var userId      = jwtUtils.getUserId(headerAuth);
    
        // Params
        var titre = req.body.titre;
        var description = req.body.description;
        var type =req.body.type;
        var datePub = req.body.datePub;
        var id =req.params.id

        asyncLib.waterfall([
          function(donne) {
            models.Livres.findOne({
                attributes: [ 'id','titre', 'description','type','datePub' ],
                where: { id: id }
            }).then(function (listLivre) {
              donne(null, listLivre);
            })
            .catch(function(err) {
              return res.status(500).json({ 'erreur': "Incapable de verifier le livre" });
            });
          },
          
          function(listLivre,donne) {
            if(listLivre) {
              listLivre.update({
                titre: titre, description:description,type:type,datePub:datePub
              }).then(function() {
                donne(listLivre);
                //console.log(userFound);
              }).catch(function(err) {
                //console.log(err);
                res.status(500).json({ 'erreur': "Impossible de modifier livre" });
              });
            } else {
              res.status(404).json({ 'erreur': 'livre non trouver' });
            }
          },
        ], function(listLivre) {
          if (listLivre) {
            return res.status(201).json(listLivre);
          } else {
            return res.status(500).json({ 'erreur': "Impossible de modifier  livre" });
          }
        

        });
      }


}