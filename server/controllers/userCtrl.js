// import
var bcrypt = require('bcrypt');
var jwtUtils = require('../utils/jwt.utils');
var models = require('../models');
var asyncLib = require('async');

//const validation email
const EMAIL_REGEX =/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const PASSWD_REGEX =/^(?=.{8,8})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^!&*+=]).*$/;

//Routes
module.exports = {
    //Enregistrer utilisateur
    registre: function (req,res){
        // param
        var nom = req.body.nom;
        var prenom = req.body.prenom;
        var courriel = req.body.courriel;
        var motPasse = req.body.motPasse;

        //verifier le parametre null
        if(nom == "" || prenom == ""){
            return res.status(400).json({'erreur':' parammetre vide'});
        }else if(!EMAIL_REGEX.test(courriel)){
            //verifier courriel incorrecte
            return res.status(400).json({'erreur':'Email invalide'});
        }else if(!PASSWD_REGEX.test(motPasse) || motPasse.length != 8){
            //verifier mots de passe  incorecte
            return res.status(400).json({'erreur':' Mots de passe invalide( doit contenir 8 caractères dont au moins 1 majuscule, 1 minuscule et 1 caractère spécial) '});
        } else{
            asyncLib.waterfall([
                //founction select utilisateur 
                function(donne){
                    models.Users.findOne({
                        attributes:['courriel'],
                        where:{courriel: courriel}
                    })
                    .then(function(userFound) {
                        donne(null, userFound);
                      })
                    .catch(function(err){
                        return res.status(500).json({'erreur':'Incapable de verifier utilisateur'});
                    });
                },
                //Fonction Crypter mot de passe
                function(userFound,donne){
                    if (!userFound) {
                        bcrypt.hash(motPasse, 5, function(err, bcryptMotPasse) {
                          donne(null, userFound, bcryptMotPasse);
                        });
                    } else {
                        return res.status(409).json({'erreur': 'utilisateur existe déjà!'});
                    }
                },
                //fonction creer utilisateur
                function(userFound, bcryptMotPasse, donne) {
                    var newUser = models.Users.create({
                        nom: nom,
                        prenom:prenom,
                        courriel:courriel,
                        motPasse: bcryptMotPasse
                    })
                    .then(function(newUser){
                        donne(newUser);
                    })
                    .catch(function(err){
                        return res.status(500).json({'erreur':'Ajout d utilisateur imposible'});
                    });
                }
            ],function(newUser){
                    if(newUser){
                        return res.status(201).json({ 'userId': newUser.id });
                    }else{
                        return res.status(500).json({'erreur':'Ajout d utilisateur imposible'});
                    }
                }
            );

        }

        
    },
    
    //fonction connecter l'utilisateur
    login: function(req,res){
        
        //param
        var courriel = req.body.courriel;
        var motPasse = req.body.motPasse;

        //verifier le parametre null
        if(courriel == null|| motPasse == null){
            return res.status(400).json({'erreur':' parammetre vide'});
        }else if(!EMAIL_REGEX.test(courriel)){
            return res.status(400).json({'erreur':'Email invalide'});
        }else if(!PASSWD_REGEX.test(motPasse) || motPasse.length !=8){
            return res.status(400).json({'erreur':' Mots de passe invalide (Doit contenir 8 caractères dont au moins 1 majuscule, 1 minuscule et 1 caractère spécial) '});
        }else{

            asyncLib.waterfall([
                function(donne){
                    models.Users.findOne({
                        where:{courriel: courriel}
                    })
                    .then(function(usersFound){
                        donne(null,usersFound);
                    })
                    .catch(function(err){
                        console.log("Incapable de verifier utilisateur");
                        return res.status(500).json({'erreur':'Incapable de verifier utilisateur'});
                    });
                },
                //Fonction verifier users is exist
                function(usersFound, donne){
                    if(usersFound){
                        bcrypt.compare(motPasse, usersFound.motPasse, function(errBcrypt,resBcrypt){
                            donne(null, usersFound,resBcrypt);
                        });
                    }else{
                        console.log("l'utilisateur n'existe pas");
                        return res.status(404).json({"erreur": "l'utilisateur n'existe pas"});
                    }
                },
                function(usersFound, resBcrypt, donne){
                    if(resBcrypt){
                        donne(usersFound);
                    }else{
                        console.log("Mots de passe invalide");
                        return res.status(403).json({"erreur":"Mots de passe invalide"});
                    }
                }
            ], function(usersFound){
                if(usersFound){
                    console.log(usersFound.id);
                    return res.status(200).json({
                        'userId': usersFound.id,
                        'token': jwtUtils.genererTokenUser(usersFound)
                    });
                }else{
                    console.log("Mots de passe invalide");
                    return res.status(403).json({"erreur":"Mots de passe invalide"});
                }
            }); 
        }
    },
    
    //fonction get utilisateur connecté
    getUserActif: function(req,res){
         // Getting auth header
        var headerAuth  = req.headers['authorization'];
        var userId      = jwtUtils.getUserId(headerAuth);

        //console.log(headerAuth);

        if (userId < 0)
            return res.status(400).json({ 'erreur': 'erreur token' });

        models.Users.findOne({
            attributes: [ 'id', 'courriel', 'nom', 'prenom' ],
            where: { id: userId }
        })
        .then(function(oneUser) {
            if (oneUser) {
                res.status(201).json(oneUser);
            } else {
                res.status(404).json({ 'erreur': 'utilisateur non touver' });
            }
            })
        .catch(function(err) {
            res.status(500).json({ 'erreur': 'Impossible de récupérer utilisateur' });
        });
      
    },

    getListUser: function(req,res){
        models.Users.findAll({
            attributes: [ 'id', 'courriel', 'nom', 'prenom' ]
        })
        .then(function(listUser) {
            if (listUser) {
                res.status(201).json(listUser);
            } else {
                res.status(404).json({ 'erreur': 'utilisateur non touver' });
            }
            })
        .catch(function(err) {
            res.status(500).json({ 'erreur': 'Impossible de récupérer utilisateur' });
        });
    },

    updateUser: function(req, res) {
        // Get auth header
        var headerAuth  = req.headers['authorization'];
        //var userId      = jwtUtils.getUserId(headerAuth);
    
        // Params
        var motPasse = req.body.motPasse;
        var courriel = req.body.courriel;
        /*
        if(userId < 0){
            console.log("erreur token");
            return res.status(400).json({ 'erreur': 'erreur token' });
        }*/

        if(!PASSWD_REGEX.test(motPasse) || motPasse.length != 8){
            //verifier mots de passe  incorecte
            return res.status(400).json({'erreur':' Mots de passe invalide( doit contenir 8 caractères dont au moins 1 majuscule, 1 minuscule et 1 caractère spécial) '});
        }

        asyncLib.waterfall([
          function(donne) {
            models.Users.findOne({
                attributes: [ 'id','courriel', 'motPasse' ],
                where: { courriel: courriel }
            }).then(function (userFound) {
              donne(null, userFound);
            })
            .catch(function(err) {
              return res.status(500).json({ 'erreur': "Incapable de verifier l'utilisateur" });
            });
          },
          function(userFound,donne){
            bcrypt.hash(motPasse, 5, function(err, bcryptMotPasse) {
                donne(null, userFound, bcryptMotPasse);
            });
            
           },
          function(userFound, bcryptMotPasse,donne) {
            if(userFound) {
              userFound.update({
                motPasse: (bcryptMotPasse ? bcryptMotPasse : userFound.motPasse)
              }).then(function() {
                donne(userFound);
                //console.log(userFound);
              }).catch(function(err) {
                //console.log(err);
                res.status(500).json({ 'erreur': "Impossible de modifier l'utilisateur" });
              });
            } else {
              res.status(404).json({ 'erreur': 'utilisateur non trouver' });
            }
          },
        ], function(userFound) {
          if (userFound) {
            return res.status(201).json(userFound);
          } else {
            return res.status(500).json({ 'erreur': "Impossible de modifier  l'utilisateur" });
          }
        });
      }


}