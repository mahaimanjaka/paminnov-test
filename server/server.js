//import
var express = require("express");
var bodyParser = require("body-parser");
var cors = require("cors");
var apiRouter = require('./apiRouter').router;
var app = express();

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors());

// configuration bodyParser pour content-type - application/json
app.use(bodyParser.json());

// configuration bodyParser pour content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/api', apiRouter);

// simple route
/*app.get("/", (req, res) => {
  res.json({ message: "Welcome to bezkoder application." });
});*/

// set port, listen for requests
var PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});