'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Livres extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      models.Livres.belongsTo(models.Users, {
        foreingKey:{
          allowNull:false
        }
      })

    }
  };
  Livres.init({
    titre: DataTypes.STRING,
    description: DataTypes.STRING,
    couverture: DataTypes.STRING,
    id_user: DataTypes.INTEGER,
    datePub: DataTypes.DATE,
    type: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Livres',
  });
  return Livres;
};